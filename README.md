# dell-xps13-9370-wifi-fix

bash script to backup current dell xps13 9370 Qualcomm Atheros QCA6174 wifi drivers and replace with specified driver

### Usage
This clones the latest drivers for the QCA6174 wifi card located here: https://github.com/kvalo/ath10k-firmware/tree/master/QCA6174/hw3.0/4.4.1

Find the highest numbered one (00132 as of this writing - firmware-6.bin_WLAN.RM.4.4.1-00132-QCARMSWP-1) and set that in line 4 of the script:
`firmware_name='firmware-6.bin_WLAN.RM.4.4.1-00132-QCARMSWP-1'`
