#!/bin/bash

# filename of latest firmware
firmware_name='firmware-6.bin_WLAN.RM.4.4.1-00132-QCARMSWP-1'

# clone down latest version of drivers to /tmp
cd /tmp
git clone https://github.com/kvalo/ath10k-firmware.git

# get current timestamp with format YYYY-MM-DD_HH:MM:SS
timestamp=$(date +%Y-%m-%d_%H:%M:%S)

# backup firmware with timestamp to avoid clobbering old backups
mv /lib/firmware/ath10k/QCA6174/hw3.0/board-2.bin /lib/firmware/ath10k/QCA6174/hw3.0/board-2.bin.bak.$timestamp
mv /lib/firmware/ath10k/QCA6174/hw3.0/firmware-6.bin /lib/firmware/ath10k/QCA6174/hw3.0/firmware-6.bin.bak.$timestamp

# copy updated firmware
cp /tmp/ath10k-firmware/QCA6174/hw3.0/board-2.bin /lib/firmware/ath10k/QCA6174/hw3.0/board-2.bin
cp /tmp/ath10k-firmware/QCA6174/hw3.0/4.4.1/$firmware_name /lib/firmware/ath10k/QCA6174/hw3.0/firmware-6.bin

# clean up
rm -rf /tmp/ath10k-firmware

# use fwupd to check for and apply any firmware updates
systemctl start fwupd
fwupdmgr refresh
fwupdmgr update

